Hn = require('../models/hn.model');
var Request = require('request');

const queryUrl = 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs';

exports.index = (req, res) => {
    Hn.find().sort({created_at: -1}).find((err, hns) => {
        if (err) {
            res.json({
                status: 'error',
                message: err
            });
        }
        res.json({
            status: 'success',
            message: 'Hn retrieved successfully',
            data: hns
        });
    });
};

exports.delete = function (req, res) {
    Hn.remove(
        {
            _id: req.params.hn_id
        },
        (err, hn) => {
            if (err) res.send(err);
            res.json({
                status: "success",
                message: "Hn deleted"
            });
        }
    );
};

exports.getDataUrl = () => {
    Request.get(queryUrl, (err, response, body) => {
        if (err) {
            res.json({
                status: 'error',
                message: err
            });
        }
        const data = JSON.parse(body).hits;

        for (let hnRes of data) {
            Hn.findOneAndUpdate({story_id: hnRes.story_id}, hnRes, (err, result) => {
                if (!result) {
                    let hn = new Hn(hnRes);
                    hn.save(function(err, doc) {
                        if (err) return console.error(err);
                        console.log("Hn inserted succussfully!");
                    });
                }
            });
        }
    });
};
