var mongoose = require('mongoose');

var hnSchema = mongoose.Schema({
    created_at: {
        type: Date
    },
    title: {
        type: String
    },
    url: {
        type: String
    },
    author: {
        type: String
    },
    story_id: {
        type: Number
    },
    story_title: {
        type: String
    },
    story_url: {
        type: String
    },
    comment_text: {
        type: String
    },
    num_comments: {
        type: Number
    }
});

const Hn = (module.exports = mongoose.model('hn', hnSchema));
