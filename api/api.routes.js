let router = require('express').Router();

router.get('/', (req, res) => {
  res.json({
    status: 'API Its Working',
    message: 'Welcome to HN Feed!'
  });
});

var hnController = require('./controllers/hn.controller');

router
  .route('/hns')
  .get(hnController.index);
router
  .route("/hn/:hn_id")
  .delete(hnController.delete);

  module.exports = router;