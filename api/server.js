require("dotenv").config();

let express = require('express');
let app = express();

var schedule = require('node-schedule');

let cors = require("cors");
const environment = require('./config/environment');

let bodyParser = require('body-parser');
let mongoose = require('mongoose');

var hnController = require('./controllers/hn.controller');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// MongoDB connection
console.log('connection string', environment.mongodb.uri);
console.log('secret', environment.secret);
mongoose.connect(environment.mongodb.uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.Promise = global.Promise;

const mongoConnection = mongoose.connection
// On connection error
mongoConnection.on('error', (error) => {
  console.log('Database error: ', error);
});

// On successful connection
mongoConnection.on('connected', () => {
  console.log('Connected to database');
});

// Import routes
let apiRoutes = require('./api.routes');

// Use Api routes in the App
app.use('/api', apiRoutes);

const HOST = '0.0.0.0';

hnController.getDataUrl();

var j = schedule.scheduleJob('0 0 */1 * * *', () => {
  hnController.getDataUrl();
});

// start server
const server = app.listen(process.env.EXPRESS_PORT || 3000, HOST, () => {
  const PORT = server.address().port;
  console.log(`Running  on http://${HOST}:${PORT}`);
});
