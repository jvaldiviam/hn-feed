module.exports = {
    mongodb: {
      uri:
        'mongodb://' +
        process.env.MONGO_DB_HOST +
        (process.env.MONGO_DB_PORT
          ? ':' + process.env.MONGO_DB_PORT + '/'
          : '/') +
        process.env.MONGO_DB_DATABASE
    }
};