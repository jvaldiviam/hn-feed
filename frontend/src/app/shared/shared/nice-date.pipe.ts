import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'niceDatePipe'
})
export class NiceDatePipe implements PipeTransform {

  transform(value: string): unknown {
    const date = new Date(value),
          diff = (((new Date()).getTime() - date.getTime()) / 1000),
          daydiff = Math.floor(diff / 86400);

    if (isNaN(daydiff) || daydiff < 0)
        return '';

    const datePipe = new DatePipe("en-US");

    if (daydiff == 0 ) return diff < 86400 && datePipe.transform(value, 'shortTime'); 
    else if (daydiff == 1) return 'Yesterday';
    else return daydiff < 31 ? datePipe.transform(value, 'mediumDate') : datePipe.transform(value, 'MMM-dd-yyyy');
  }
}
