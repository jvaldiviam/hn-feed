import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NiceDatePipe } from './nice-date.pipe';

@NgModule({
  declarations: [
    NiceDatePipe
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    NiceDatePipe
  ]
})
export class SharedModule { }