import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Hn } from '../hn-interface';
import { HnService } from '../hn.service';

@Component({
  selector: 'app-hn-list',
  templateUrl: './hn-list.component.html',
  styleUrls: ['./hn-list.component.scss']
})
export class HnListComponent implements OnInit, OnDestroy {

  hns$: BehaviorSubject<Hn[]>;

  getAllSubcription: Subscription;

  constructor(private hnService: HnService) {
    this.hns$ = new BehaviorSubject([]);
  }

  ngOnInit(): void {
    this.getAll();
  }

  ngOnDestroy() {
    this.getAllSubcription && this.getAllSubcription.unsubscribe();
  }

  getAll() {
    this.getAllSubcription = this.hnService.getAll().subscribe(
      (data) => {
        this.hns$.next(data);
      },
      (error) => {}
    );
  }

  verifyHn(hn: Hn) {
    return hn.story_title || hn.title;
  }

  deleteHnByIndex(event: any, i: number) {
    const newValues = this.hns$.value;
    newValues.splice(i, 1);
    this.hns$.next([...newValues]);
  }
}
