import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import { map, catchError } from "rxjs/operators";

import { environment } from "../../../environments/environment";
import { throwError } from "rxjs";
import { Injectable } from '@angular/core';
import { Hn } from './hn-interface';

@Injectable()
export class HnService {

    constructor(private http: HttpClient) {}

    getAll() {
        return this.http.get<Hn[]>(environment.apiEndpoint + '/hns').pipe(
            map((res: any) => res.data),
            catchError(this.handleErrorObservable)
        );
    }

    delete(_id: string) {
        return this.http.delete(environment.apiEndpoint + '/hn/' + _id).pipe(
            map((res: any) => res.data),
            catchError(this.handleErrorObservable)
        )
    }

    private handleErrorObservable(error: HttpErrorResponse) {
        return throwError(error);
    }
}