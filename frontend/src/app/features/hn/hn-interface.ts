export interface Hn {
    _id: string;
    created_at: Date;
    title: string;
    url: string;
    author: string;
    story_id: number;
    story_title: string;
    story_url: string;
    comment_text: string;
    num_comments: number;
}
