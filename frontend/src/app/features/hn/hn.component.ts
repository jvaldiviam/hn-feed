import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hn',
  templateUrl: './hn.component.html',
  styleUrls: ['./hn.component.scss']
})
export class HnComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get title() {
    return 'HN Feed';
  }

  get description() {
    return 'We <3 hacker news!';
  }

}
