import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HnComponent } from './hn.component';

const routes: Routes = [
    {
        path: "",
        component: HnComponent
    }
];
  
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HnRoutingModule { }
