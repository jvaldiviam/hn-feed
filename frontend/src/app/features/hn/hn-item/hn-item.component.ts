import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Hn } from '../hn-interface';
import { HnService } from '../hn.service';

@Component({
  selector: 'app-hn-item',
  templateUrl: './hn-item.component.html',
  styleUrls: ['./hn-item.component.scss']
})
export class HnItemComponent implements OnInit, OnDestroy {

  @Input() hn: Hn;
  @Output() onDelete = new EventEmitter<boolean>();

  deleteSubscription: Subscription;

  constructor(private hnService: HnService) { }

  ngOnInit(): void { }

  ngOnDestroy() {
    this.deleteSubscription && this.deleteSubscription.unsubscribe();
  }

  get title() {
    return this.hn.story_title || this.hn.title;
  }

  get author() {
    return '- ' + this.hn.author + ' -';
  }

  get date() {
    return this.hn.created_at;
  }

  onClickItem() {
    window.open("//" + (this.hn.story_url || this.hn.url), '_blank');
  }
  
  deleteHn() {
    this.deleteSubscription = this.hnService.delete(this.hn._id).subscribe(
      (data) => {
        this.onDelete.next(true);
        alert('Hn deleted');
      },
      (error) => {}
    );
  }
}
