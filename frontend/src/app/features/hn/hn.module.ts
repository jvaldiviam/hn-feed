import { NgModule } from '@angular/core';
import { HnComponent } from './hn.component';
import { HnRoutingModule } from './hn-routing.module';
import { HnListComponent } from './hn-list/hn-list.component';
import { HnService } from './hn.service';
import { HnItemComponent } from './hn-item/hn-item.component';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { MatIconModule } from '@angular/material/icon'; 
import { MatButtonModule } from '@angular/material/button'; 

@NgModule({
  declarations: [
    HnComponent,
    HnListComponent,
    HnItemComponent
  ],
  imports: [
    SharedModule,
    HnRoutingModule,
    MatIconModule,
    MatButtonModule
  ],
  providers: [
    HnService
  ]
})
export class HnModule { }
