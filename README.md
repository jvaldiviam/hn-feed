# HN-Feed using Docker

This project is made of MongoDB, Express, Angular and NodeJS. All these components are running using docker for development.

![Screenshot](frontend.png)

## Table of Contents
* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
    * [Using Docker](#using-docker)
      * [Prerequisites](#prerequisite)
    * [Without Docker](#without-docker)
      * [Prerequisites](#prerequisites)

### About Project

This is a simple web application. 
The server should take care of pulling the data into the database and expose an API for theAngular client. The client should render a web page that lists the articles in chronological order

### Built With
#### Angular

The frontend of this project is developed in Angular.

It contains sample for below:

 1. Hack News Interface
 2. Get and Delete Hack New

Also, It has sample code for services, etc.

##### [Dockerfile](/frontend/Dockerfile)

#### Expressjs

All rest services are developed using express js.

It constains sample for:

1. Mongodb connection and schema validation using Mongoose
2. API routing
5. Get and Delete for Hack News

##### [Dockerfile](/api/Dockerfile)

#### Mongo DB

MongoDB is a cross-platform document-oriented database program.

##### [Database user creation script](/mongo/init-mongo.sh)

## Getting started

#### Using Docker

##### Prerequisite
Install latest [Docker Desktop](https://www.docker.com/products/docker-desktop)

You can start the application (database, api and frontend) using docker-compose:


  ```
  git clone https://gitlab.com/jvaldiviam/hn-feed.git
  cd hn-feed
  
  docker-compose up
  ```

  It will run fronend `http://localhost:4200`  and api on `http://localhost:3000/api` . you can also access mongodb on port 27017. 

#### Without Docker

##### Prerequisites

1. Install latest [Node js ](https://nodejs.org/en/)
2. Install Nodemon as global package
   `npm install -g nodemon`
3. Install Angular CLI
   `npm install -g @angular/cli`)
4. Install Mongodb locally.

#### Running the Project

Clone the project and run `npm install` in frontend and api folder.

```
  git clone https://gitlab.com/jvaldiviam/hn-feed.git
  
  cd hn-feed/frontend
  npm install
  npm start

  cd hn-feed/api
  npm install
  npm start 
  ```
It will run Api on `http://localhost:3000/api` and frontend on `http://localhost:4200`


## About Docker Compose File ([docker-compose.debug.yml](/docker-compose.yml))

  ```dockerfile
  version: "3.8"

services:
  angular:
    build: frontend
    container_name: hn-feed-frontend
    ports:
      - "4200:4200"
    volumes:
      - ./frontend:/frontend
      - /frontend/node_modules

  express:
    build: api
    container_name: hn-feed-api
    ports:
      - "3000:3000"
    volumes:
      - ./api:/api
      - /api/node_modules
    environment:
      - MONGO_DB_USERNAME=admin-hn-feed
      - MONGO_DB_PASSWORD=admin-hn-feed-password
      - MONGO_DB_HOST=mongo
      - MONGO_DB_PORT=27017
      - MONGO_DB_DATABASE=hn-feed
    links:
      - mongo

  mongo:
    image: mongo
    container_name: hn-feed-mongo
    ports:
      - "27017:27017"
    environment:
      - MONGO_DB_USERNAME=admin-hn-feed
      - MONGO_DB_PASSWORD=admin-hn-feed-password
      - MONGO_DB_DATABASE=hn-feed
    volumes:
      - ./mongo:/home/mongodb
      - ./mongo/init-db.d/:/docker-entrypoint-initdb.d/
      - ./mongo/db:/data/db
  ```